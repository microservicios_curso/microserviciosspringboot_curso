package com.vaxi.springbootmicroservice1inmueble.model;

import lombok.Data;

import javax.persistence.*;

import java.time.LocalDateTime;

//lombok se usa para resolver los getter y setter
@Data
@Entity
@Table(name="inmueble")
public class Inmueble {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", length = 150, nullable = false)
    private String nombre;

    @Column(name = "direccion", length = 500, nullable = false)
    private String direccion;

    @Column(name="foto", length = 1200, nullable = false)
    private String picture;

    @Column(name = "precio", nullable = false)
    private Double precio;

    @Column(name = "fecha_creacion", nullable = false)
    private LocalDateTime fechaCreacion;
}
