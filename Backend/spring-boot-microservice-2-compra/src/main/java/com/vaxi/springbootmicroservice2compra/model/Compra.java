package com.vaxi.springbootmicroservice2compra.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data //agregará get and set en tiempo de ejecución
@Entity
@Table(name="compra")
public class Compra {

    //esta es la entidad de compra

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // si sale Long en rojo te vas a file invalidate cache y selecciona el primer checkbox

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "inmueble_id", nullable = false)
    private Long inmuebleId;

    @Column(name = "titulo", nullable = false)
    private String titulo;

    @Column(name = "precio", nullable = false)
    private Double precio;

    @Column(name = "fecha_compra", nullable = false)
    private LocalDateTime fechaCompra;

}
