
# Tests

def test_validation1():
    f = FoodDeliverySystem()
    expected_result = {
        1: {"customer_name": "Mary Smith", "order_items": {"Burger": 1, "Pasta": 2}, "status": "Placed"}
    }
    assert f.place_order("Mary Smith", {"Burger": 1, "Pasta": 2}) == expected_result, "Test failed"

def test_validation():
    f = FoodDeliverySystem()
    expected_result = {
        "Burger": 150,
        "Pizza": 250,
        "Pasta": 200,
        "Salad": 120,
        "Beverages": 130,
        "Noodles": 150,
        "Sushi": 270,
        "Bakery": 350
    }
    assert f.display_menu() == expected_result, "Test failed"


def test_validation3():
    f = FoodDeliverySystem()
    # Asumiendo que existe un pedido con order_id = 1
    result = f.pickup_order(1)
    expected_result = {
        1: {"customer_name": "ABC", "order_items": {"item1": "Quantity"}, "status": "Picked Up"}
    }
    assert result == expected_result, "Test failed"

def test_validation4():
    f = FoodDeliverySystem()
    # Asumiendo que existe un pedido con order_id = 2
    f.place_order("John Doe", {"Pizza": 3, "Salad": 1})
    result = f.deliver_order(2)
    assert result == "not delivered", "Test failed"

def test_validation5():
    f = FoodDeliverySystem()
    # Asumiendo que existe un pedido con order_id = 3
    f.place_order("Alice Johnson", {"Noodles": 2, "Beverages": 4})
    new_items = {"Bakery": 1, "Pizza": 2}
    result = f.modify_order(3, new_items)
    expected_result = {
        3: {"customer_name": "Alice Johnson", "order_items": {"Noodles": 2, "Beverages": 4, "Bakery": 1, "Pizza": 2}, "status": "Placed"}
    }
    assert result == expected_result, "Test failed"

# Ejecutar los tests
test_validation1()
test_validation()
test_validation3()
test_validation4()
test_validation5()
