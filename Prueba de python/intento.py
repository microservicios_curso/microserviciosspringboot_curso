class FoodDeliverySystem:
    order_id = 0
    orders_log = {}

    def __init__(self):
        self.menu = {
            "Burger": 150,
            "Pizza": 250,
            "Pasta": 200,
            "Salad": 120,
            "Beverages": 130,
            "Noodles": 150,
            "Sushi": 270,
            "Bakery": 350
            # Agrega más elementos al menú si es necesario
        }
        self.bill_amount = 0

    def display_menu(self):
        # La implementación de este método permanece igual

    def place_order(self, customer_name, order_items):
        FoodDeliverySystem.order_id += 1
        order_id = FoodDeliverySystem.order_id

        for item in order_items:
            if item not in self.menu:
                return "order placement failed"

        FoodDeliverySystem.orders_log[order_id] = {
            "customer_name": customer_name,
            "order_items": order_items,
            "status": "Placed"
        }

        return FoodDeliverySystem.orders_log

    def pickup_order(self, order_id):
        if order_id in FoodDeliverySystem.orders_log and FoodDeliverySystem.orders_log[order_id]["status"] == "Placed":
            FoodDeliverySystem.orders_log[order_id]["status"] = "Picked Up"
            return FoodDeliverySystem.orders_log[order_id]
        else:
            return "order placement failed"

    def deliver_order(self, order_id):
        if order_id in FoodDeliverySystem.orders_log and FoodDeliverySystem.orders_log[order_id]["status"] == "Picked Up":
            FoodDeliverySystem.orders_log[order_id]["status"] = "Delivered"
            return "Delivered"
        else:
            return "not delivered"

    def modify_order(self, order_id, new_items):
        if order_id in FoodDeliverySystem.orders_log and FoodDeliverySystem.orders_log[order_id]["status"] == "Placed":
            # Modifica los elementos del pedido solo si el pedido no se ha recogido
            FoodDeliverySystem.orders_log[order_id]["order_items"].update(new_items)
            return FoodDeliverySystem.orders_log[order_id]
        else:
            return "order placement failed"

    def generate_bill(self, order_id):
        if order_id in FoodDeliverySystem.orders_log:
            order_details = FoodDeliverySystem.orders_log[order_id]
            items = order_details["order_items"]
            total_amount = sum(self.menu[item] * items[item] for item in items)

            if total_amount > 1000:
                tax = 0.1  # 10% de impuesto
            else:
                tax = 0.05  # 5% de impuesto

            total_bill_amount = total_amount + (tax * total_amount)

            return total_bill_amount
        else:
            return "order placement failed"

    def cancel_order(self, order_id):
        if order_id in FoodDeliverySystem.orders_log and FoodDeliverySystem.orders_log[order_id]["status"] == "Placed":
            del FoodDeliverySystem.orders_log[order_id]
            return FoodDeliverySystem.orders_log
        else:
            return "order placement failed"
