import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomeRoutingModule } from './welcome-routing.module';
import { WelcomeComponent } from './welcome.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    WelcomeComponent
  ],
  imports: [
    MatCardModule,
    CommonModule,
    WelcomeRoutingModule
  ]
})
export class WelcomeModule { }
