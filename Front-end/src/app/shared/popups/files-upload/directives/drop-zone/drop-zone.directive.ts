import { Directive, EventEmitter, Output, HostListener } from '@angular/core';

@Directive({
  selector: '[appDropZone]'
})
export class DropZoneDirective {

  @Output() dropper = new EventEmitter<FileList>();
  @Output() hovered = new EventEmitter<boolean>();

  constructor() { }

  @HostListener('drop',['$event'])
  onDrop($event:any){
    $event.preventDefault();
    this.dropper.emit($event.dataTransfer.files);
    this.hovered.emit(false);
  }

  @HostListener('dragover',['$event'])
  onDragOver($event:any){
    $event.preventDefault();
    this.hovered.emit(true);
  }

  @HostListener('dragleave',['$event'])
  onDragLeave($event:any){
    $event.preventDefault();
    this.hovered.emit(false);

  }

}
