import { Token } from '@angular/compiler';
import {User} from '@app/models/backend/user';
//interface que representa un objeto de tipo response para hacer login
export {User as UserResponse} from '@app/models/backend/user'

//interface que representa un objeto de tipo request
export interface EmailPasswordCredentials{
  //modelo para hacer la transaccion del login
  email: string;
  password: string;
}


export interface UserRequest extends User {
  password: string;
}

export type UserCreateRequest = Omit<UserRequest, 'token' | 'id'>;
