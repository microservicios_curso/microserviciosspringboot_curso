import { Injectable } from '@angular/core';
import * as fromActions from './user.actions'
import { HttpClient } from '@angular/common/http';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NotificationService } from '@app/services';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { UserResponse } from './user.models';
//import { environment } from 'environments/environment';
import { environment } from '@src/environments/environment';

type Action = fromActions.All;

@Injectable()
export class UserEffects {
  //http client de Angular
  //intancia de los objetos a utilizar para la conexion con el backend
  constructor(
    private httpClient: HttpClient,
    private actions: Actions,
    private notifications: NotificationService,
    private router: Router,
  ) { }

  signUpEmail: Observable<Action> = createEffect(() =>
    this.actions.pipe(
      //indicar el action a ejecutar
      ofType(fromActions.Types.SIGN_UP_EMAIL),
      map((action: fromActions.SignUpEmail) => action.user),
      switchMap(userData =>
        //revisar las ` ` en caso de que sean las otras
        //this.httpClient.post<UserResponse>(`${environment.url}account/register/`, userData) //TEMPORAL
        this.httpClient.post<UserResponse>(`${environment.url}api/authentication/sign-up`, userData) //PARA EL SERVIDOR
          .pipe(//este pipe evaluara el posible resultado que obtenga del backend
            tap((response: UserResponse) => { // registrara en el browser el token de seguridad
              localStorage.setItem('token', response.token);
              this.router.navigate(['/']);//redireccionara a la pagina principal
            }),
            //el map devuelve el observable que necesitamos
            map((response: UserResponse) => new fromActions.SignUpEmailSuccess(response.email, response || null)),
            catchError(err => {
              this.notifications.error("Errores al registrar un nuevo usuario");
              return of(new fromActions.SignUpEmailError(err.message))
            })
          )
      )
    )
  );

  signInEmail: Observable<Action> = createEffect(() =>
    this.actions.pipe(
      //indicar el action a ejecutar
      ofType(fromActions.Types.SIGIN_IN_EMAIL), // 1RO define la operación a trabajar
      map((action: fromActions.SignInEmail) => action.credentials), // 2DA obtener parametros para llevar acabo la transacción (operacion)
      switchMap(userData => // 3RO representa la comunicación con el servidor y la evaluación de los resultados
        //revisar las ` ` en caso de que sean las otras
        //this.httpClient.post<UserResponse>(`${environment.url}account/login-app/`, userData)
        this.httpClient.post<UserResponse>(`${environment.url}api/authentication/sign-in`, userData)
          .pipe(//este pipe evaluara el posible resultado que obtenga del backend
            tap((response: UserResponse) => { // registrara en el browser el token de seguridad
              localStorage.setItem('token', response.token);
              this.router.navigate(['/']);//redireccionara a la pagina principal
            }),
            //el map devuelve el observable que necesitamos
            map((response: UserResponse) => new fromActions.SignInEmailSuccess(response.email, response || null)),
            catchError(err => {
              this.notifications.error("Las credenciales no son las correctas");
              return of(new fromActions.SignInEmailError(err.message))
            })
          )
      )
    )
  );

  Init: Observable<Action> = createEffect(() =>
    this.actions.pipe(
      //indicar el action a ejecutar
      ofType(fromActions.Types.INIT), // 1RO define la operación a trabajar
      switchMap(async () => localStorage.getItem('token')),
      switchMap(token => {
        if (token){
          //return this.httpClient.get<UserResponse>(`${environment.url}account/session/`)
          return this.httpClient.get<UserResponse>(`${environment.url}api/user/`)
          .pipe(//este pipe evaluara el posible resultado que obtenga del backend
            tap((response: UserResponse) => { // registrara en el browser el token de seguridad
             console.log('datos de usuario en session que viene del servidor', response)
            }),
            //el map devuelve el observable que necesitamos
            map((response: UserResponse) => new fromActions.InitAuthorized(response.email, response || null)),
            catchError(err => {
              return of(new fromActions.InitError(err.message))
            })
          )
        }
        else{
          return of(new fromActions.InitUnauthorized());
        }
      })
    )
  );
}


