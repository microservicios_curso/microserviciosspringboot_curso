import { Action } from "@ngrx/store";
import { EmailPasswordCredentials, UserCreateRequest, UserResponse } from "./user.models";

// Metodos Actions
export enum Types {
  //Operacion para saber si el usuario esta en session
  INIT = '[User] Init: Start',
  INIT_AUTHORIZED = '[User] Init: Authorized',
  INIT_UNAUTHORIZED = '[User] Init: Unauthorized',
  INIT_ERROR = '[User] Init: Error',
  //Operacion para hace login
  SIGIN_IN_EMAIL = '[User] Login: Start',
  SIGIN_IN_EMAIL_SUCCESS = '[User] Login: Success',
  SIGIN_IN_EMAIL_ERROR = '[User] Login: Error',
  //Operacion para hace registro de un nuevo usuario
  SIGN_UP_EMAIL = '[User] Registrar usuario con Email: Start',
  SIGN_UP_EMAIL_SUCCESS = '[User] Registrar usuario con Email: Success',
  SIGN_UP_EMAIL_ERROR = '[User] Registrar usuario con Email: Error',
  //Operacion para hace logout
  SIGIN_OUT_EMAIL = '[User] Logout: Start',
  SIGIN_OUT_EMAIL_SUCCESS = '[User] Logout: Success',
  SIGIN_OUT_EMAIL_ERROR = '[User] Logout: Error',
}

// INIT para saber si el usuario esta en session
export class Init implements Action{
  readonly type = Types.INIT;
  constructor(){}
}
// INIT validar
export class InitAuthorized implements Action{
  readonly type = Types.INIT_AUTHORIZED;
  constructor(public email: string, public user: UserResponse | null){}
}
// INIT validar
export class InitUnauthorized implements Action{
  readonly type = Types.INIT_UNAUTHORIZED;
  constructor(){}
}
// INIT validar
export class InitError implements Action{
  readonly type = Types.INIT_ERROR;
  constructor(public error: string){}
}
// INIT para hacer el login
export class SignInEmail implements Action{
  readonly type = Types.SIGIN_IN_EMAIL;
  constructor(public credentials: EmailPasswordCredentials){}
}
export class SignInEmailSuccess implements Action{
  readonly type = Types.SIGIN_IN_EMAIL_SUCCESS;
  constructor(public email:string, public user: UserResponse){}
}
export class SignInEmailError implements Action{
  readonly type = Types.SIGIN_IN_EMAIL_ERROR;
  constructor(public error:string){}
}
// INIT para hacer el registro
export class SignUpEmail implements Action{
  readonly type = Types.SIGN_UP_EMAIL;
  constructor(public user: UserCreateRequest){}
}
export class SignUpEmailSuccess implements Action{
  readonly type = Types.SIGN_UP_EMAIL_SUCCESS;
  constructor(public email: string, public user: UserResponse | null){}
}
export class SignUpEmailError implements Action{
  readonly type = Types.SIGN_UP_EMAIL_ERROR;
  constructor(public error: string){}
}
// INIT para hacer el logout
export class SignOut implements Action{
  readonly type = Types.SIGIN_OUT_EMAIL;
  constructor(){}
}
export class SignOutSuccess implements Action{
  readonly type = Types.SIGIN_OUT_EMAIL_SUCCESS;
  constructor(){}
}
export class SignOutError implements Action{
  readonly type = Types.SIGIN_OUT_EMAIL_ERROR;
  constructor(public error:string){}
}
export type All =
            Init
          | InitAuthorized
          | InitError
          | InitUnauthorized
          | SignInEmail
          | SignInEmailError
          | SignInEmailSuccess
          | SignUpEmail
          | SignUpEmailError
          | SignUpEmailSuccess
          | SignOut
          | SignOutError
          | SignOutSuccess

